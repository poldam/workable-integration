## Integrates Workable in Wordpress ##

## Shortcode ##

[estros-workable bootstrapcss=1 titlesize=30 btncss='btnclass'][/estros-workable] 

If bootstrap attribute is set to 1 then the required css from bootstrap for the modals will be included (just what is necessary for the modals and nothing more).

**ATTENTION!!:**Make sure you have included the appropriate bootstrap.js or that it is already included by your theme.

**btncss** attribute defines the class for formatting the apply button.

**titlesize** attribute defines the size of the title in the jobs listing.