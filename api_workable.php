<?php

class WorkableAPI
{
	public function add_candidate($job_shortcode, $firstname, $lastname, $headline, $summary, $address, $phone, $email, $foto_url, $resume_url, $answers)
	{ 
		$url = "https://www.workable.com/spi/v3/accounts/".esc_attr(get_option('subdomain'))."/jobs/".$job_shortcode."/candidates";
		$method = 'POST';
		$headers = array(
			'Content-Type: application/json',
			"Authorization:Bearer ".esc_attr(get_option('token'))
		);
		
		$data = '
			{
				"candidate": {
					"name": "'.$firstname.' '.$lastname.'",
					"firstname": "'.$firstname.'",
					"lastname": "'.$lastname.'",
					"headline": "'.$headline.'",
					"summary": "'.$summary.'",
					"address": "'.$address.'",
					"phone": "'.$phone.'",
					"email": "'.$email.'",
					"sourced": false,
					'.$answers.',
					"resume_url": "'.$resume_url.'"';
					if($foto_url != "")
					{
						$data .= ', 
					"image_url": "'.$foto_url.'"';
					}
			$data .='
				}
			}';
		
		$response_data = $this->workable_request($url, $method, $data, $headers);		
		//echo $data;
		return $response_data;		
	}
	
	public function workable_request($url, $method, $data, $headers)
	{    
		$handle = curl_init();
		
		curl_setopt($handle, CURLOPT_URL, $url);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_HEADER, true);
		curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($handle, CURLOPT_VERBOSE, FALSE);
		curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($handle, CURLOPT_TIMEOUT, 90);
	
		switch ($method) {	
			case 'GET':
					break;
			case 'POST':
					curl_setopt($handle, CURLOPT_POST, true);
					curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
					break;
			case 'PUT':
					curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT');
					curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
					break;
			case 'DELETE':
					curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE');
					break;
		}
		
		$response = curl_exec($handle);
		$code     = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($handle, CURLINFO_HEADER_SIZE);
		$body        = substr($response, $header_size);
		$error       = json_decode($body);
		$response_data    = Array();
		$response_data[0] = $code;
		$response_data[1] = $error;
		return $response_data;
	}
}
?>