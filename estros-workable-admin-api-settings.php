<?php

if (isset($_POST['workable_prefs']))
{
	update_option('token', $_POST['token']);	
	update_option('subdomain', $_POST['subdomain']);
}
?>
<h3>Preferences</h3>

<form name="workableform" method="POST" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
  <strong>API Key:</strong><br />
  <input type="text" value="<?php echo esc_attr( get_option('token') ); ?>" id="token" name="token" ><br />
  <strong>Subdomain:</strong><br />
  <input type="text" value="<?php echo esc_attr( get_option('subdomain') ); ?>" id="subdomain" name="subdomain" >.workable.com<br />
	<input type="hidden" value="YES" id="workable_prefs" name="workable_prefs" >
  <?php submit_button("Save Preferences"); ?>
  <br />
</form>
