<?php
/**
 * Plugin Name: Estros Workable API
 * Plugin URI: http://estros.gr
 * Description: Integrates Workable with Wordpress.
 * Version: 1.0
 * Author: Estros.gr
 * Author URI: http://estros.gr
 * License: GPL2
 */

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define( 'BASE_PATH', dirname(__FILE__) . '/' );

require_once('api_workable.php');

function workableform($atts, $content){
  $buttontext = "Sign me up!";
  if(!empty($content))
    $buttontext = $content;
  extract( shortcode_atts( array(
    'bootstrapcss' => '0',
		'bootstrapjs' => '0',
		'btncss' => '',
		'titlesize' => 16,
		'id' => '0000',
  ), $atts, 'estros-workable' ) );
	
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	$workable = new WorkableAPI();
	
	$headers = array(
		'Content-Type: application/json',
		"Authorization:Bearer ".esc_attr(get_option('token'))
	);
	$url = "https://www.workable.com/spi/v3/accounts/".esc_attr(get_option('subdomain'))."/jobs?state=published";
	$data = "";
	$method = "GET";
	$response = $workable->workable_request($url, $method, $data, $headers);
	//////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////
	
	$jobs = $response[1]->jobs;
	
	$i = 0;
	?>
	<script type="application/javascript">
	jQuery(function(){
		
		jQuery('p').each(function() {
			if(jQuery(this).html().replace(/\s|&nbsp;/g, '').length == 0)
					jQuery(this).remove();
		});
		
	});
	
	function wisValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((\"[\w-+\s]+\")|([\w-+]+(?:\.[\w-+]+)*)|(\"[\w-+\s]+\")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
	};
	
	function wcheckFirstname(code)
	{
		if(jQuery('#' + code + 'wfirstname').val().length < 3)
			jQuery('#' + code + 'wfirstname').addClass('wError');
		else
			jQuery('' + code + '#wfirstname').removeClass('wError');
	}
	
	function checkQs(code)	
	{
		jQuery('.w' + code + 'question').each(function() 
		{
			if (jQuery(this).val() == '') 
				jQuery(this).addClass('wError');
			else
				jQuery(this).removeClass('wError');
		});
	}
	
	function wcheckPhone(code)
	{
		if(jQuery('#' + code + 'wphone').val().length < 10)
			jQuery('#' + code + 'wphone').addClass('wError');
		else
			jQuery('#' + code + 'wphone').removeClass('wError');
	}
	
	function wcheckLastname(code)
	{
		if(jQuery('#' + code + 'wlastname').val().length < 3)
			jQuery('#' + code + 'wlastname').addClass('wError');
		else
			jQuery('#' + code + 'wlastname').removeClass('wError');
	}
	
	function wcheckEmail(code)
	{
		if(!wisValidEmailAddress(jQuery('#' + code + 'wemail').val()))
			jQuery('#' + code + 'wemail').addClass('wError');
		else
			jQuery('#' + code + 'wemail').removeClass('wError');
	}
	
	function wsubmitForm(code) {
		var errors = '';
		
		var emailaddress = jQuery('#' + code + 'wemail').val();
		
		var firstname = jQuery('#' + code + 'wfirstname').val();
		var lastname = jQuery('#' + code + 'wlastname').val();
		var phone = jQuery('#' + code + 'wphone').val();
		
		if(phone.length < 10)
		{
			errors += "The phone field is mandatory and it should be at least 10 digits.\n";
			jQuery('#' + code + 'wphone').addClass('wError');
		}
		
		if(firstname.length < 3)
		{
			errors += "Firstname must be at least 3 characters.\n";
			jQuery('#' + code + 'wfirstname').addClass('wError');
		}
		if(lastname.length < 3)
		{
			errors += "Lastname must be at least 3 characters.\n";
			jQuery('#' + code + 'wlastname').addClass('wError');
		}
		 
		if(!wisValidEmailAddress(emailaddress))
		{
			errors += "Email address must be valid (myemail@email.com).\n";
			jQuery('#' + code + 'wemail').addClass('wError');
		}
		
		var allQFilled = true;

    jQuery('.w' + code + 'question').each(function() 
		{
        if (jQuery(this).val() == '') 
				{
					//alert(jQuery(this).attr('name') + ": '" + jQuery(this).val() + "'");
					if(allQFilled)
						errors += "All questions must be filled.\n";
					allQFilled = false;
					jQuery(this).addClass('wError');
        }
    });
		
		if(errors != '')
			alert(errors);
		else
		{		
			var fd = new FormData();
			var foto = jQuery('#' + code + 'form input[type="file"]')[0].files[0];
			fd.append("foto", foto);
			
			var resume = jQuery('#' + code + 'form input[type="file"]')[1].files[0];
			fd.append("resume", resume);
			
			var other_data = jQuery('#' + code + 'form').serializeArray();
			jQuery.each(other_data,function(key,input){
					fd.append(input.name, input.value);
			});
			jQuery.ajax({
				url: '<?php echo plugin_dir_url( __FILE__ )."addRecord.php"; ?>',
				data: fd,
				contentType: false,
				processData: false,
				type: 'POST',
				success: function(result){
					if(result == 200 || result == 201)
					{
						jQuery('.modal').modal('hide');
						alert('Thank You for Applying!');
					}
					else if(result == 422)
						alert('This email is already used for applying!');
					else if(result == 400 || result == 401)
						alert('ERROR: Information is not passed on properly! Please try again!');
					else
						alert('ERROR: ' + result);
				},
				error: function(result){
					alert('An error occured while processing your application!\nPlease try again later!');
				}
			});
		}
	}
	</script>
	
	<?php

	echo '<div id="accordion" class="panel-group">';
	foreach($jobs as $job)
	{
		$url = "https://www.workable.com/spi/v3/accounts/".esc_attr(get_option('subdomain'))."/jobs/".$job->shortcode;
		$specificjob = $workable->workable_request($url, $method, $data, $headers);
		
		//echo "<pre>";
			//print_r($specificjob[1]);
		//echo "</pre>"; 
		
		echo '
		<h4 style="font-size:'.$titlesize.'px; padding: 20px; background-color: #efefef; border-radius: 6px;">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$i.'">'.$specificjob[1]->full_title." (REF: ".$specificjob[1]->shortcode.')</a>
    </h4>
    <div id="collapse'.$i.'" class="panel-collapse collapse workablepanel" style="padding:20px; padding-left:30px; padding-right: 30px; border-radius: 6px; background-color: #efefef;">';
			echo "<div><strong>Location:</strong> ".$specificjob[1]->location->city.", ".$specificjob[1]->location->region.", ".$specificjob[1]->location->country."</div>";
			echo "<div><strong>Required Experience:</strong> ".$specificjob[1]->experience."</div>";
			if(!empty($specificjob[1]->education))
			echo "<div><strong>Education Level:</strong> ".$specificjob[1]->education."</div>";
			echo "<div><strong>Employment Type:</strong> ".$specificjob[1]->employment_type."</div>";
			echo "<div><strong>Description:</strong><div style='font-size: 14px;text-align: justify; word-break: normal; word-wrap: normal; hyphens: none;-webkit-hyphens: none; -moz-hyphens: none;'>
			".$specificjob[1]->description."</div></div>";
			echo "<div><div style='font-size: 14px;text-align: justify; word-break: normal; word-wrap: normal; hyphens: none;-webkit-hyphens: none; -moz-hyphens: none;'>".$specificjob[1]->requirements."</div></div>";
			echo "<div><h3>Benefits:</h3><div style='font-size: 14px;text-align: justify; word-break: normal; word-wrap: normal; hyphens: none;-webkit-hyphens: none; -moz-hyphens: none;'>
			".$specificjob[1]->benefits."</div></div>";
		?>
    <br>
      <input type="submit" class="<?php echo $btncss; ?>" value="Apply for this Job!" data-toggle="modal" data-target="#apppopupform-<?php echo $job->shortcode; ?>">
      
      <div id="apppopupform-<?php echo $job->shortcode; ?>" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center"><?php echo $specificjob[1]->full_title." (REF: ".$specificjob[1]->shortcode; ?>)</h4>
            </div>
            <div class="modal-body">
							<form id="<?php echo $job->shortcode; ?>form" class="form" name="<?php echo $job->shortcode; ?>form" onsubmit="return false;">
              <div class="wsectionTitle">Personal Information</div>
              	<div class='wqblock'>
                	<div class='wqtitle'>
                  	*First Name
                  </div>
                  <input type="text" id="<?php echo $job->shortcode; ?>wfirstname" name="<?php echo $job->shortcode; ?>wfirstname" class="wtextfield" onchange="wcheckFirstname('<?php echo $job->shortcode; ?>');"/>
                </div>
               	<div class='wqblock'>
                  <div class='wqtitle'>
                  	*Last Name
                  </div>
                  <input type="text" id="<?php echo $job->shortcode; ?>wlastname" name="<?php echo $job->shortcode; ?>wlastname" class="wtextfield" onchange="wcheckLastname('<?php echo $job->shortcode; ?>');"/>
                </div>
                <div class='wqblock'>
                  <div class='wqtitle'>
                  	*Email
                  </div>
                  <input type="text" id="<?php echo $job->shortcode; ?>wemail" name="<?php echo $job->shortcode; ?>wemail" class="wtextfield" onchange="wcheckEmail('<?php echo $job->shortcode; ?>');"/>
                </div>
                <div class='wqblock'>
                  <div class='wqtitle'>
                  	Headline
                  </div>
                  <input type="text" id="<?php echo $job->shortcode; ?>wheadline" name="<?php echo $job->shortcode; ?>wheadline" class="wtextfield"/>
                </div>
                <div class='wqblock'>
                  <div class='wqtitle'>
                  	*Phone
                  </div>
                  <input type="text" id="<?php echo $job->shortcode; ?>wphone" name="<?php echo $job->shortcode; ?>wphone" class="wtextfield" onchange="wcheckPhone('<?php echo $job->shortcode; ?>');"/>
                </div>
                <div class='wqblock'>
                  <div class='wqtitle'>
                  	Address
                  </div>
                  <input type="text" id="<?php echo $job->shortcode; ?>waddress" name="<?php echo $job->shortcode; ?>waddress" class="wtextfield"/>
                </div>
                <div class='bigwqblock'>
                  <div class='wqtitle'>
                  	Photo
                  </div>
                  <input type="file" id="<?php echo $job->shortcode; ?>foto" name="<?php echo $job->shortcode; ?>foto" class="wtextfield"/>
                  <div class="wsmalltext">Please provide an image in PNG, JPG or JPEG format. Max file size 900 Kb.</div>
                </div>
                <hr />
                <div class="wsectionTitle">Your Profile</div>
                <div class='bigwqblock'>
                  <div class='wqtitle'>
                  	Summary
                  </div>
                  <textarea id="<?php echo $job->shortcode; ?>wsummary" name="<?php echo $job->shortcode; ?>wsummary" class="wtextfield"></textarea>
                </div>
                <div class='bigwqblock'>
                  <div class='wqtitle'>
                  	*Resume
                  </div>
                  <input type="file" id="<?php echo $job->shortcode; ?>resume" name="<?php echo $job->shortcode; ?>resume" class="wtextfield" />
                  <div class="wsmalltext">Please provide your CV in pdf, word, odt, html or rtf document format. Max file size 900 Kb.</div>
                </div>
                <hr />
                <div class="wsectionTitle">Application Details</div>
								<?php
                  $url = "https://www.workable.com/spi/v3/accounts/".esc_attr(get_option('subdomain'))."/jobs/".$job->shortcode."/questions";
                  $questions = $workable->workable_request($url, $method, $data, $headers);
                  //print_r($questions[1]->questions);
                  foreach($questions[1]->questions as $question)
                  {
										echo "<div class='bigwqblock'>";
                    echo "<div class='wqtitle'>*".$question->body."</div>";
										if($question->type == 'free_text')
                    	echo "<textarea id='".$question->id."".$question->id."' name='".$question->id."--q' class='wtextfield w".$job->shortcode."question' onchange='checkQs(\"".$job->shortcode."\");'></textarea>";
										else if($question->type == 'multiple_choice')
										{
											foreach($question->choices as $qoption)
											{
												$tempname = "checkbox";

												if($qoption->single_answer == true)
													$tempname = "radio";
													
												echo '<input type="'.$tempname.'" name="'.$question->id.'--q[]'.'" id="'.$qoption->id.'" value="'.$qoption->id.'" class="wradio"> <span class="wradio">'.$qoption->body.'</span><br>';
											}
										}
										else if($question->type == 'boolean')
										{
											echo '<input type="radio" name="'.$question->id.'--q[]'.'" value="false" class="" checked> <span class="wradio wno">NO</span> &nbsp;&nbsp;&nbsp;';
											echo '<input type="radio" name="'.$question->id.'--q[]'.'" value="true" class=""> <span class="wradio wyes">YES</span><br>';
										}
										echo "</div>";
                  }
                ?>
                <div class='bigwqblock text-center'>
                	<br />
								  <input type="hidden" name="token" id="token" value="u7y3cohysyieeqDT3y9t1hVdwdwp32szhIsXlXdW7HTFh1" />
									<input type="hidden" name="job_shortcode" id="<?php echo $job->shortcode; ?>job_shortcode" value="<?php echo $specificjob[1]->shortcode; ?>" />
                	<input type="submit" class="<?php echo $btncss; ?>" value="Apply for this Job!" id="<?php echo $i; ?>" onclick="wsubmitForm('<?php echo $job->shortcode; ?>');"/>
                </div>
							</form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
		$i++;
	}
	echo '</div>';
}

function workable_scripts_important()
{
  wp_enqueue_script('jquery');
  wp_register_style( 'workableCSS', plugins_url( '/css/estros-workable.css', __FILE__ ), array(), '20120208', 'all' );
  wp_enqueue_style( 'workableCSS' );
}

 
function workable_admin(){
  add_menu_page( 'Workable Integration', 'My Workable', 'administrator', 'estros-workable', 'workable_init', 'dashicons-id-alt' );
}

add_action('admin_menu', 'workable_admin');
 
function workable_init(){
  ?>
  <div class="wrap">
    <div style="padding:5px; display: inline-block; width:42%;">   
    <h2>Workable Integration Plugin</h2>
      <div class="welcome-panel">
		<a href="https://estros.gr" target="_blank">
          <img src="http://estros.gr/logo.png" height="45">
        </a>
        <div style="width:100%; font-size: 18px; font-weight: bold;">Plugin Description</div>
        <p>Integrates Workable in Wordpress..</p> 
      </div>
      <div class="welcome-panel">
        <?php
        if($_GET['settings-updated'])
        {
        ?>
          <div style="font-size: 12px; border: 2px #1EA1CC solid;padding:2px;background-color: #2EA2CC; color: white;">Your settings have been saved</div><br>
        <?php
        }
        ?>
        <?php include 'estros-workable-admin-api-settings.php'; ?>
      </div>
  	</div>
    <div class="welcome-panel" style="vertical-align: top; width: 53%; display: inline-block;">
      <div>
      	<h3>Current API Key</h3>
        <?php 
					$tovisit = "<your subdomain>";
					if(!empty(get_option('subdomain')))
						$tovisit = esc_attr(get_option('subdomain'));
					$tovisit = "API KEY is not set yet! Please visit ".$tovisit.".workable.com/backend/account/integrations to get your key!";
					if(!empty(get_option('token')))
						echo esc_attr(get_option('token'));
					else
						echo $tovisit;
				?>
        <br /><br />
        <h3>Current Subdomain</h3>
        <?php
        	if(!empty(get_option('subdomain')))
						echo esc_attr(get_option('subdomain')).".workable.com";
					else
						echo "Subdomain is not set yet!";
				?>
      </div>
      <div>
      <br />
      	<h3> Shortcode</h3>
        <p>
        <code>
        		[estros-workable bootstrapcss=1 titlesize=30 btncss='btnclass'][/estros-workable] 
        </code>
        </p>
        <p>
          If bootstrap attribute is set to 1 then the required css from bootstrap for the modals will be included (just what is necessary for the modals and nothing more).<br /><br />
          <strong>ATTENTION!!:</strong> Make sure you have included the appropriate bootstrap.js or that it is already included by your theme.
        </p>
        <p>
        	<strong>btncss</strong> attribute defines the class for formatting the apply button.
        </p>
        <p>
        	<strong>titlesize</strong> attribute defines the size of the title in the jobs listing.
        </p>
      </div>
  	</div>
  </div>
  <?php
}

add_action( 'admin_init', 'workable_plugin_settings' );
function workable_plugin_settings() {
  register_setting( 'workable-plugin-settings-group', 'token' );
	register_setting( 'workable-plugin-settings-group', 'subdomain' );
}
add_action( 'wp_enqueue_scripts', 'workable_scripts_important', 5 );
add_shortcode('estros-workable', 'workableform');