<?php
//print_r($_POST);
if(isset($_POST['token']) && $_POST['token'] == "u7y3cohysyieeqDT3y9t1hVdwdwp32szhIsXlXdW7HTFh1")
{
	require_once('../../../wp-config.php');
	
	require_once('api_workable.php');
	
	$workable = new WorkableAPI();
	
	$errors = 0;
	$emailErr = "";
	
	$job_shortcode = $_POST['job_shortcode'];
	
	$email = $_POST[$job_shortcode.'wemail'];
	$firstname = $_POST[$job_shortcode.'wfirstname']; 
	$lastname = $_POST[$job_shortcode.'wlastname'];

	$headline = $_POST[$job_shortcode.'wheadline'];
	$summary = $_POST[$job_shortcode.'wsummary'];
	$address = $_POST[$job_shortcode.'waddress'];
	$phone = $_POST[$job_shortcode.'wphone'];

	if(strlen($firstname) < 3)
	{
		$emailErr .= "Firstname must be at least 3 characters.\n";
		$errors++;
	}
	
	if(strlen($lastname) < 3)
	{
		$emailErr .= "Lastname must be at least 3 characters.\n";
		$errors++;
	}
	
	if(empty($_FILES["resume"]["name"]))
	{
		$emailErr .= "Please Upload your resume.\n";
		$errors++;
	}

//$errors = 8;

	$fotoextensions = array('PNG', 'png', 'jpg', 'jpeg', 'JPG', 'JPEG');
	$resumeextensions = array('pdf', 'PDF', 'doc', 'DOC', 'docx', 'DOCX', 'html', 'HTML', 'rtf', 'RTF', 'odt', 'ODT');
	
	$fotoext = "";
	$resext= "";
	
	if(!empty($_FILES["foto"]["name"]))
	{
		$temp = explode(".", $_FILES["foto"]["name"]);
		$fotoext = $temp[sizeof($temp) - 1];
		if(!in_array($fotoext, $fotoextensions))
		{
			$emailErr .= "Your image format is not valid.\nPlease provide an image in PNG, JPG or JPEG format.\n";
			$errors++;
		}
		if($_FILES["foto"]['size'] > 1000000)
		{
			$emailErr .= "Your image file size shouldn't be more than 900 KB.\n";
			$errors++;
		}
	}
	
	$temp = explode(".", $_FILES["resume"]["name"]);
	$resext = $temp[sizeof($temp) - 1];
	if(!in_array($resext, $resumeextensions))
	{
		$emailErr .= "Your resume format is not valid.\nPlease provide your CV in pdf, word, odt, html or rtf document format.\n";
		$errors++;
	}
	if($_FILES["resume"]['size'] > 1000000)
	{
		$emailErr .= "Your resume file size shouldn't be more than 900 KB.\n";
		$errors++;
	}
	
	if($errors == 0)
	{
		//echo "\n\n";
		$answers = '"answers": [';	
		$check = 0;
		foreach($_POST as $key => $value)
		{
			if(strpos($key,'--q') === false)
			{
				
			}
			else
			{
				$qtype = "choices";
				$cleankey = str_replace("--q","", $key);
				if(is_array($_POST[$key]))
				{
					if(!empty($_POST[$key][0]) && ($_POST[$key][0] == "true" || $_POST[$key][0] == "false"))
						$qtype = "checked";
					if($check > 0)
						$answers .= ',';
					$answers .= '
					{
						"question_key": "'.$cleankey.'",
						"'.$qtype.'":';
					if($qtype == "choices")	
						$answers .= '[';
					$ch = 0;
					foreach($_POST[$key] as $k => $v)
					{
						if($ch > 0)
							$answers .= ',';
						if($qtype == "checked")	
							$answers .= ''.$v.'';
						else
							$answers .= '"'.$v.'"';
						$ch++;
					}
					if($qtype == "choices")	
						$answers .= ']';
					$answers .='
					}';
				}
				else
				{
					if($check > 0)
						$answers .= ',';
					$answers .= '
					{
						"question_key": "'.$cleankey.'",
						"body": "'.$value.'"
					}';
				}
				$check++;
			}
		}
		$answers .= ']';
		//echo $answers;
		//echo "\n\n";
		
		$random = rand(0, 9999999);
		$random2 = rand(888, 909090);
		$myrand = $random."".$random2;
		if(!empty($_FILES["foto"]["name"]))
		{
			$foto_url = plugin_dir_url( __FILE__ )."uploads/"."".$myrand."".$_FILES["foto"]["name"];
			move_uploaded_file($_FILES["foto"]["tmp_name"], "uploads/"."".$myrand."".$_FILES["foto"]["name"]);
		}
		else
			$foto_url= "";
		
		$resume_url = plugin_dir_url( __FILE__ )."uploads/"."".$myrand."".$_FILES["resume"]["name"];
		move_uploaded_file($_FILES["resume"]["tmp_name"], "uploads/"."".$myrand."".$_FILES["resume"]["name"]);
		
		$response_data = $workable->add_candidate($job_shortcode, $firstname, $lastname, $headline, $summary, $address, $phone, $email, $foto_url, $resume_url, $answers); 
		unlink($resume_url);
		if(!empty($_FILES["foto"]["name"]))
			unlink($foto_url);
			
		echo $response_data[0];
	}
	else
	{
		echo "Fields are not filled in properly!\n".$emailErr;
		//print_r($_POST);
		//echo "\n============================\n";
		//echo "Foto link: ".$foto_url."\n";
		//echo "Resume link: ".$resume_url;
		//echo "\n============================\n";
		//print_r($_FILES);
	}
}
else
{
	echo "Indirect access of the form is not allowed!";
}
?>